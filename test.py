import os
import argparse

parser=argparse.ArgumentParser("test run python script")
parser.add_argument("input_argument1", default=0)
args= parser.parse_args()

print("--in python test.py")
print("argument1: ", args.input_argument1)

with open("test_input.txt", "r") as test_input:
	intext=test_input.readlines()

with open("test_output.txt", "w") as test_output:
	test_output.write("lalala\n")
	for input_text in intext:
		test_output.write("%s"%input_text)	

print("---Done running--.")
